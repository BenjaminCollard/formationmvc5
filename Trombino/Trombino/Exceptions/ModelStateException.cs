﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Trombino.Exceptions
{
    public class ModelStateException : Exception
    {
        public ModelStateException(ModelStateDictionary modelState)
            : base(BuildMessage(modelState))
        {

        }

        private static string BuildMessage(ModelStateDictionary modelState)
        {
            StringBuilder builder = new StringBuilder();
            foreach (var kvp in modelState)
            {
                builder.AppendLine(kvp.Key + " ");
                kvp.Value.Errors.Select(e => e.ErrorMessage).ToList().ForEach(msg =>
                {
                    builder.AppendLine(msg);
                });
            }
            return builder.ToString();
        }
    }
}