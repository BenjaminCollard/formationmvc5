﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Trombino.Models;

namespace Trombino.Controllers
{
    public class MetadatasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Metadatas
        public ActionResult Index()
        {
            return View(db.Metadata.ToList());
        }

        // GET: Metadatas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Metadata metadata = db.Metadata.Find(id);
            if (metadata == null)
            {
                return HttpNotFound();
            }
            return View(metadata);
        }

        // GET: Metadatas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Metadatas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Type,IsRequired")] Metadata metadata)
        {
            var context = new ApplicationDbContext();
            var allUsers = context.Users.ToList();

            if (ModelState.IsValid)
            {
                db.Metadata.Add(metadata);
                db.SaveChanges();

                try
                {
                    foreach (ApplicationUser user in allUsers)
                    {
                        MetadataLink metadataLink = new MetadataLink();
                        metadataLink.MetadataId = metadata.Id;
                        metadataLink.UserId = user.Id;
                        metadataLink.Value = "NULL";

                        db.MetadataLink.Add(metadataLink);
                        db.SaveChanges();
                    }
                }
                catch (DbEntityValidationException ex)
                {
                    StringBuilder sb = new StringBuilder();

                    foreach (var failure in ex.EntityValidationErrors)
                    {
                        sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                        foreach (var error in failure.ValidationErrors)
                        {
                            sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                            sb.AppendLine();
                        }
                    }

                    throw new DbEntityValidationException(
                        "Entity Validation Failed - errors follow:\n" +
                        sb.ToString(), ex
                    ); // Add the original exception as the innerException
                }
               


                return RedirectToAction("Index");
            }

            return View(metadata);
        }

        // GET: Metadatas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Metadata metadata = db.Metadata.Find(id);
            if (metadata == null)
            {
                return HttpNotFound();
            }
            return View(metadata);
        }

        // POST: Metadatas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Type,IsRequired")] Metadata metadata)
        {
            if (ModelState.IsValid)
            {
                db.Entry(metadata).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(metadata);
        }

        // GET: Metadatas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Metadata metadata = db.Metadata.Find(id);
            if (metadata == null)
            {
                return HttpNotFound();
            }
            return View(metadata);
        }

        // POST: Metadatas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Metadata metadata = db.Metadata.Find(id);
            db.Metadata.Remove(metadata);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
