﻿using System.ComponentModel.DataAnnotations;

namespace Trombino.Models
{
    public class Metadata
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public MetadataType Type { get; set; }

        [Required]
        public bool IsRequired { get; set; }
    }

    public enum MetadataType
    {
        Integer,
        String,
        File,
        Enum
    }

   
}