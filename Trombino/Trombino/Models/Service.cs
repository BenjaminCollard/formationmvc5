﻿using System.ComponentModel.DataAnnotations;

namespace Trombino.Models
{
    public class Service
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

    }
}