﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Trombino.Models
{
    public class MetadataLink
    {
        public int Id { get; set; }

        
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        [Required]
        public string UserId { get; set; }

        
        [ForeignKey("MetadataId")]
        public virtual Metadata Metadata { get; set; }

        [Required]
        public int MetadataId { get; set; }

        [Required]
        public string Value { get; set; }

    }
}