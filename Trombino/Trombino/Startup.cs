﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Trombino.Startup))]
namespace Trombino
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
