﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Trombino.Models;

namespace Trombino
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ApplicationDbContext.UpdateDatabaseToLatestVersion();

            addAdmin();
        }

        private void addAdmin()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            try
            {
                if (!roleManager.RoleExists("Admin"))
                {
                    roleManager.Create(new IdentityRole("Admin"));
                }

                var hasher = new PasswordHasher();
                var user = new ApplicationUser
                {
                    UserName = "admin@chloro.eu",
                    PasswordHash = hasher.HashPassword("Okjv1234*"),
                    Email = "admin@chloro.eu"
                };

                userManager.Create(user, "Okjv1234*");
                userManager.AddToRole(user.Id, "Admin");
            }
            catch (Exception)
            {
            }
        }
    }
}
