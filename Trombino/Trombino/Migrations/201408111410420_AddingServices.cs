namespace Trombino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingServices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "Service_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Service_Id");
            AddForeignKey("dbo.AspNetUsers", "Service_Id", "dbo.Services", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Service_Id", "dbo.Services");
            DropIndex("dbo.AspNetUsers", new[] { "Service_Id" });
            DropColumn("dbo.AspNetUsers", "Service_Id");
            DropTable("dbo.Services");
        }
    }
}
